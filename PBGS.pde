/*
 * This software is part of Blobscanner Processing library examples.
 * Hand detection and gesture recognition 
 * (c) Antonio Molinaro 2011 http://code.google.com/p/blobscanner/.
 *
 * Based on Background subtraction Class for processing 
 * by Andrew Senior   www.andrewsenior.com.
 *  
 */

class PBGS{ 
  PImageOperations PImOps = new PImageOperations();  // For performing image operations.
  int[] background;  // Background pixels.
  PImage difference;  // The image of the difference.
  
  // Create the background model to look like the starting frame.
  public PBGS(PImage img){
      background = new int[img.width * img.height*3]; // 3 layers (for RGB)
      difference = createImage(img.width, img.height, RGB);
      arrayCopy(img.pixels, difference.pixels);
      set(img);
  }
    
  // Set the background image to the given image
  void set(PImage img){
    for(int i = 0; i < img.width * img.height; i++){
        color iP = img.pixels[i];
        background[i*3] = (int)red(iP);
        background[i*3+1] = (int)green(iP);
        background[i*3+2] = (int)blue(iP);
    }
       
  }
  
  void PutDifference(){
    PImOps.Copy(imgDiff,  difference);
  }
  
  // Update the model if we chose a new background and compute the difference
  void update(PImage img){
    // Update difference.
      difference = createImage(img.width, img.height, RGB);
      arrayCopy(img.pixels, difference.pixels);
      
      for(int i=0; i<img.width*img.height; i++){
        color iP=img.pixels[i];
 
        difference.pixels[i]=color(abs( background[i*3]-red(iP)), abs( background[i*3+1]-green(iP)), abs( background[i*3+2]-blue(iP))); 
      }
      
      PImOps.Threshold( difference, 30 );
      PImage T=new PImage( difference.width,  difference.height);
      PImOps.Copy(T,  difference);
      PImOps.MorphClose(T,  difference);
      PImOps.MorphOpen(T, difference);
      pconcomps  pcc = new pconcomps();
      pcc.Compute( difference);
      pcc.RemoveSmall(18);
      pcc.SetImage( difference);
  } 
}

