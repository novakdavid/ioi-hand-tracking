************************
Navodila za uporabo
************************


Namestitev:
************************

1. Namestitev knjižnic: mapi blobscanner in controlP5 moraš prekopirati v direktorij s knjižnicami. Na Macu je to v /Users/username/Processing/libraries, na Windows je verjetno tudi mapa Processing v MyDocuments. Če v tej mapi Processing ni mape 'libraries', jo ustvari.

2. V processingu odpri hand_gesture_recognition.pde

Uporaba:
************************

Aplikacija deluje tako, da zaznava roko in prste. To naredi tako, da odšteje ozadje, ki ga vidi na začetku (ali po resetu s preslednico) od trenutne slike. Zato:

1. Zaženi aplikacijo
2. Kamero usmeri najbolje v strop oziroma vsaj tako visoko, da tvoje glave ni na kameri, sicer bo premikanje glave motilo algoritem. 
3. Ko se slika na kameri naloži pritisni preslednico/space, da se resetira računalniški vid. Spodaj lahko tudi vidiš kaj kamera zaznava. V primeru čudnega zaznavanja lahko kadarkoli znova resetiraš računalniški vid s preslednico. Pomembno pa je, da takrat ko resetiraš, nimaš roke v vidnem polju kamere oz jo umakneš.
4. Pričneš risati z roko. Uporabi roko stisnjeno v pest z iztegnjenim kazalcem za risanje. V kolikor aplikacija zazna več prstov (npr dva), potem ne riše (uporabno npr za premikanje po sliki).
5. Lahko brišeš s 5 iztegnjenih prstov, vendar je to malce težavno, saj briše vsak prst posebej ;)

Dodatno:
************************

V nastavitvah aplikacije pod platnom imaš kup dodatnih opcij. Lahko nastavljaš:

1. Resetiranje kamere (preslednica)
2. Brisanje celotnega platna (C)
3. Nastavljanje debeline čopiča z drsnikom. Lahko uporabiš tudi tipki + in -
4. Nastavljanje hitrosti pojemanja barve. Zatem ko barvo namočiš v paleto se ta z risanjem porablja in peša. S tem drsnikom lahko nastaviš kako hitro se to dogaja. 1 pomeni da peša počasi, 100 pa da z vsakim izrisanim pikslom porabiš 10% barve. (10 izrisanih pikslov ni veliko).
5. Ponovno namakanje čopiča v isto barvo - da ti ni potrebno hoditi na rob v paleto vsakič ko ti zmanka barve (če je ne želiš zamenjati), lahko to storiš z gumbom spodaj oziroma s tipko R na tipkovnici.
6. Barve menjaš tako da greš do roba in se zapelješ v okence palete glede na to katero barvo bi rad ;)
7. Imaš tudi opcijo free-hand-mode. S to opcijo rišeš s konicami vseh prstov (do 4), če imaš pa iztegnjenih vseh 5 prstov pa brišeš (tudi s konicami).