class FingerDetector{
  PImage out;
  
  public FingerDetector(){
    //out = createImage(w, h, RGB);
  }
  
  // Sets the image for this detector instance.
  void setImage(PImage img){
    if(out == null){
      out = createImage(img.width, img.height, RGB);
    }
    
    out.loadPixels();
    for(int k = 0; k < img.pixels.length; k++){
      out.pixels[k] = img.pixels[k];
    }
    out.updatePixels();
  }
  
  // ***Checks if the input pixel is a finger pixel.***
  // Takes the coordinates of a pixel. For each pixel on the circumference
  // with the radius 120/2*Pi and the center at the chosen pixel, count how 
  // many of those belong to a hand, based on the brightness of the blob.
  boolean isFingerPixel(int x, int y){
    int numOfWhite = 0;
    float r =  120 /TWO_PI;
    
    // To detect a finger, the number of found pixels must be between 0 and 8.
    // Its scale is inversely proportional to the distance of the hand from the camera.
    int finger_size = 8; 
    
    // For each pixels on the circumference with radius 120/2*pi and with center 
    // located at x,y (current pixel's coordinate) ...
    for(int k = 0; k < 360; k+=10){
      // Current coordinates of the pixel located at the circumference.
      int cx =  x + (int)(r*cos(k));
      int cy =  y + (int)(r*sin(k));
   
      if(cx >= 0 && cx  < out.width && cy  >= 0 && cy  < out.height){  // Pixel must be within the bounds of the image.
        // How many pixels are a hand pixel.
        if(brightness((out.pixels[cx+cy*out.width]&0xff0000)>>16) == 255){
          numOfWhite++;
        }
      }
    }
     
    if(numOfWhite != 0 && numOfWhite < finger_size){
      return true;  // Found a finger tip pixel at these coordiantes.
    }
    return false;
  }
}
