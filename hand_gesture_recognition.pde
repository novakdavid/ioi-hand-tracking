import blobscanner.*;
import processing.video.*;
import controlP5.*;

/* CONFIG */
int captureWidth = 320;
int captureHeight = 240;
int canvasWidth = 812;
int canvasHeight = 610;

int mainPartHeight = max(captureWidth*2 + 20, canvasHeight);
/* DEBUG */
boolean debug = true;

/* UI */
int width = captureWidth+canvasWidth+60;
int height = mainPartHeight+40;
ControlP5 cp5;
Textlabel brushModeLabel;
int colorCellSize;

/* DISPLAY */
PFont f;  // To display feedback.
Capture frame;  // The video frame. Self-explanatory.
PBGS  pBackground;  // The background object.
PImage tips = createImage(captureWidth, captureHeight, RGB);     // Finger tips locator feedback image.
PGraphics canvas;  // The drawing board.
PImage FG = createImage(captureWidth, captureHeight,RGB); 
PImage imgDiff = createImage(captureWidth, captureHeight, RGB); 

/* DETECTORS */
FingerDetector fd;    // Finger detector instance.
Detector bs, bstips;  // Blobscanner instances.

/* COMMAND UTILITIY */
int fingersCount = 0;       // Number of active fingers. Helps us recognize commands
final int TIPS_MASS = 45;   // How much mass, to consider blob a fingertip.
final int HAND_MASS = 600;  // How much mass, to consider blob a hand.
boolean mustSetBackground = true;  // Whether the background has been set yet.

/* DRAWING */
int brushSize = 7;
color brushColor = color(255, 0, 0);
boolean freeHandMode = false;  // Enables freehand drawing. Finger shapes are brush shapes.
Toggle freeHand;
Range brushSizeRange, colorDecayRange;
float colorDecay = 1.0;

void setup(){
  f = createFont("", 15);
  textFont(f, 15);
  
  height = mainPartHeight + 120;
  size(width, height);    // Initialize main window.
  cp5 = new ControlP5(this);
  freeHand = cp5.addToggle("FREE_HAND_MODE").setPosition(captureWidth+40, mainPartHeight+50).setSize(80,30).setValue(freeHandMode)
  .setColorBackground(color(100, 100, 100)).setColorForeground(color(0, 0, 150)).setColorActive(color(0, 200, 0)).setCaptionLabel("Free hand mode [H]").setColorCaptionLabel(0);
  
  brushSizeRange = cp5.addRange("brushSizeRange")
             // disable broadcasting since setRange and setRangeValues will trigger an event
             .setBroadcast(false) 
             .setPosition(captureWidth+230, mainPartHeight+50)
             .setSize(150,30)
             .setHandleSize(20)
             .setRange(1,30)
             .setRangeValues(1,30)
             // after the initialization we turn broadcast back on again
             .setBroadcast(true)
             .setColorBackground(color(20, 80, 150))
             .setColorForeground(color(0,20, 80))  
             .setColorValueLabel(color(255,0,0))
             .setDecimalPrecision(0)
             .setLowValue(9.0)
             .showTickMarks(true)
             .setCaptionLabel("Brush size [+/-]")
             .setColorCaptionLabel(0)
             ;
             
     colorDecayRange = cp5.addRange("colorDecayRange")
             // disable broadcasting since setRange and setRangeValues will trigger an event
             .setBroadcast(false) 
             .setPosition(captureWidth+470, mainPartHeight+50)
             .setSize(150,30)
             .setHandleSize(20)
             .setRange(0,100)
             .setRangeValues(0,100)
             // after the initialization we turn broadcast back on again
             .setBroadcast(true)
             .setColorBackground(color(20, 80, 150))
             .setColorForeground(color(0,20, 80))  
             .setColorValueLabel(color(255,0,0))
             .setDecimalPrecision(0)
             .setLowValue(10)
             .showTickMarks(true)
             .setCaptionLabel("Color decay")
             .setColorCaptionLabel(0)
             ;
    cp5.addButton("reloadColor")
    .setCaptionLabel("Renew color [R]")
     .setPosition(captureWidth+700, mainPartHeight+50)
     .updateSize()
     .setSize(80,30)
     .setColorBackground(color(20, 80, 150))
     .setColorForeground(color(0,20, 80)) .setColorActive(color(0, 200, 0))
     ;
     
     cp5.addButton("clearImage")
    .setCaptionLabel("Clear image [C]")
     .setPosition(captureWidth-90, mainPartHeight+50)
     .updateSize()
     .setSize(80,30)
     .setColorBackground(color(20, 80, 150))
     .setColorForeground(color(0,20, 80)) .setColorActive(color(0, 200, 0))
     ;
     
     cp5.addButton("resetCamera")
    .setCaptionLabel("Reset camera [Space]")
     .setPosition(captureWidth-210, mainPartHeight+50)
     .updateSize()
     .setSize(100,30)
     .setColorBackground(color(20, 80, 150))
     .setColorForeground(color(0,20, 80)) .setColorActive(color(0, 200, 0))
     ;
  
  // The canvas.
  canvas = createGraphics(canvasWidth, mainPartHeight);
  canvas.beginDraw();
  canvas.background(255);  // Initialize the drawing board to white.
  canvas.fill(0);
  canvas.noStroke();
  canvas.endDraw();
  
  // Start video capture.
  frame = new Capture(this, captureWidth, captureHeight);
  frame.start();
  
  // Initializes the class for background subtraction.
  pBackground = new PBGS(FG);
  
  // Setup blob detectors.
  fd = new FingerDetector();
  bs = new Detector(this, 0, 0, captureWidth, captureHeight, 255);
  bstips = new Detector(this, 0, 0, captureWidth, captureHeight, 255);
}

// For reading the video frames.
void captureEvent(Capture video) {
  frame.read();
}
 
void draw(){
  background(200);  // UI background.
  
  //******   BACKGROUND SUBTRACTION  ******//
  frame.loadPixels();
  image(frame, 20, 20);    // Display the default video first.
  
  FG.loadPixels();
  arrayCopy(frame.pixels, FG.pixels);  // Copy the current frame.
  FG.updatePixels();
  
  if(mustSetBackground){  // Check whether the background has to be set.
    pBackground.set(FG);
    mustSetBackground = false;
  }
  
  pBackground.update(FG);    // Update the background model with the new frame.
  imgDiff.loadPixels();
  pBackground.PutDifference();    // Puts the difference in imgDiff
  imgDiff.updatePixels();
  
  //******  FINGER DETECTION  ******//
  imgDiff.loadPixels();
  fd.setImage(imgDiff);  // Use the difference image for finger detection.
  imgDiff.updatePixels();
  
  bs.imageFindBlobs(imgDiff);  // Compute blobs in BGS image
  bs.loadBlobsFeatures();
  bs.weightBlobs(false);
  
  drawFingerTips();                       
  if(debug)drawFingersTipsBoundingBox();
}
 
// Force update of the background model when a key is clicked. 
void keyPressed(){
  if(key == ' '){
    mustSetBackground = true;
  }
  
  if(key == 'c'){
    canvas.beginDraw();
    canvas.background(255);
    canvas.endDraw();
    mustSetBackground = true;
  }
  
  if(key == 'h'){
    freeHand.toggle();
  }
  
  if(key == '+'){
    brushSizeRange.setLowValue(brushSizeRange.getArrayValue(0) + 1.0);
  }
  
  if(key == '-'){
    brushSizeRange.setLowValue(brushSizeRange.getArrayValue(0) - 1.0);
  }
  
  if(key == 'r'){
    brushColor = color(red(brushColor), green(brushColor), blue(brushColor), 255);
    colorDecay = 1.0;
    System.out.println("Renewed the color.");
  }
  
}

void FREE_HAND_MODE(boolean flag) {
  freeHandMode = flag;
  if(flag == true) {
    println("Freehand mode ON.");
  } else {
    println("Freehand mode OFF.");
  }
}

/*
  This function analyzes the blobs in the
  tips image. If their mass if >= to TIPS_MASS
  a finger is added to the count.
 */
void drawUI(){
   
   fill(0);
   text(fingersCount + " finger tips detected.", 20, captureHeight*2+50);
   text("1 finger = DRAW :)", 20, captureHeight*2+70);
   text("5 fingers = ERASE :)", 20, captureHeight*2+90);
   fill(brushColor);
   stroke(255,255,255);
   rect(captureWidth+canvasWidth-20, mainPartHeight+28, 60, 60);
   
   //2*2*2 = 8
   //3*3*3 = 27
   int[] colorPoints = new int[] {0, 150, 255};
   ArrayList<Integer> availableColors = new ArrayList<Integer>();
   
   for (int r = 0; r < colorPoints.length; r ++) {
    for (int g = 0; g < colorPoints.length; g ++) {
      for (int b = 0; b < colorPoints.length; b ++) {
         availableColors.add(color(colorPoints[r], colorPoints[g], colorPoints[b]));
       }   
     }
   }
   

   canvas.beginDraw();
   float cellWidth = (float)canvasHeight/availableColors.size()*2;
   colorCellSize = (int)cellWidth;
   
   for (int i = 0; i < availableColors.size()/2; i++) {
     canvas.fill(availableColors.get(i));
     canvas.rect(0, (int)(i*cellWidth), (int)cellWidth, (int)cellWidth, 0);
   }
   
   for (int i = availableColors.size()/2; i < availableColors.size(); i++) {
     canvas.fill(availableColors.get(i));
     canvas.rect(canvasWidth-(int)cellWidth, (int)((i-availableColors.size()/2)*cellWidth), (int)cellWidth, (int)cellWidth, 0);
   }
   
   
   //canvas.stroke(0,0,0);
   //canvas.line(0, (int)cellWidth+1, canvasWidth, (int)cellWidth+1);
   
   canvas.endDraw();
   
}
 
/*
   Here many important things happen.  
   The tips image is first initialized to black.
   Then, the hand's blob is searched in the BGS image.
   Once it has been found, the blob pixels are scanned for
   possibly finger tip regions. 
   After that a finger tips image is created based upon
   the search's result data. The image is then scanned for blobs.  
*/
void drawFingerTips(){
  
  drawUI();  // Update UI.
  
  //******  PROCESS FINGER BLOBS  ******//
  tips.loadPixels();  // The image containing the info about the finger tips.
  for(int i = 0; i < captureWidth*captureHeight; i++)tips.pixels[i] = color(0);   // Initialize the image to black.
  
  canvas.beginDraw();
  canvas.loadPixels();
  
  // For each pixels in the BGS image (imgDiff) ...
  for(int y =  0; y < captureHeight; y++){
    for(int x =  0; x < captureWidth; x++){
      // First, check if it's a hand.
      if(bs.isBlob(x, y) && bs.getBlobWeightLabel(bs.getLabel(x, y)) >= HAND_MASS){
        // Second, check if it's a finger.
        if(fd.isFingerPixel(x, y)){
          // Color to white if true.
          tips.pixels[x+y*tips.width] = 0xff << 16 & 0xff0000 | 0xff << 8 & 0xff00 | 0xff & 0xff;
          // Free hand mode only.
          if(freeHandMode || fingersCount == 5){
            if(fingersCount > 0 && fingersCount < 5)
              canvas.pixels[x+y*canvas.width] = color(0);
            else if(fingersCount == 5)
              canvas.pixels[x+y*canvas.width] = color(255);  // When 5 fingers are active, clear the board.
          }
        }
      } 
    }
  }
   
  tips.updatePixels();
  canvas.updatePixels();
  canvas.endDraw();
   
  // Compute the blobs in the finger image.
  bstips.imageFindBlobs(tips);
  bstips.loadBlobsFeatures();
  bstips.weightBlobs(false);
  
  // Count the fingers in the image.
  fingersCount = 0;
   for(int i = 0; i < bstips.getBlobsNumber(); i++){
     if(bstips.getBlobWeight(i) >= TIPS_MASS) 
       fingersCount++;
  }
  

  if(debug)image(tips,20,captureHeight+30);  // Display user fingers.
  
  //******  MAKE CHANGES TO CANVAS IF BRUSH MODE  ******//
  if(!freeHandMode){
    canvas.beginDraw();
    canvas.noStroke();
    /* 
     One finger: draw on the board.
     Two fingers: change brush color to red
     Three fingers: change brush color to green
     Four fingers: change brush color to blue
     Five finger: -not assigned-
     Six fingers and more: -not assigned-
    */
    switch(fingersCount){  // Change command in correspondance to the number of fingers.
      case 0:
        break;
      case 1:
        int blobWidth, blobHeight;
        float brushX, brushY;
        for(int i = 0; i < bstips.getBlobsNumber(); i++){
          blobWidth = bstips.getBlobWidth(i);
          blobHeight = bstips.getBlobHeight(i);
          if(blobHeight > 20 || blobWidth > 20){
            brushX = bstips.getBoxCentX(i);
            brushY = bstips.getBoxCentY(i) - bstips.getBlobHeight(i)/2;
            int brushXCanvas = (int) ((captureWidth - brushX) * (float)canvasWidth/captureWidth);
            int brushYCanvas = (int) (brushY * (float)canvasHeight/captureHeight);
            
            if (brushXCanvas <= colorCellSize || canvasWidth - brushXCanvas <= colorCellSize) {
              // Select different color
              canvas.loadPixels();
              canvas.fill(color(255));
              canvas.ellipse(brushXCanvas, brushYCanvas, brushSize, brushSize);
              brushColor = canvas.pixels[brushXCanvas+brushYCanvas*canvas.width];
              colorDecay=1.0;
            } else {
              //draw
              brushColor = color(red(brushColor), green(brushColor), blue(brushColor), 255*colorDecay);
              colorDecay = colorDecay-colorDecayRange.getArrayValue(0)/1000;
              canvas.fill(brushColor);
              canvas.ellipse(brushXCanvas, brushYCanvas, brushSize, brushSize);
            }
            pushMatrix();
            translate(20, 20);
            noFill();
            stroke(255);
            ellipse(brushX, brushY, 20, 20);
            popMatrix();
          }
        }
        break;
      case 2:
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
        break;
      default:  // More than five.
        break;
    }
    // Draw feedback to which fingers are being tracked.
    int blobWidth, blobHeight;
    float brushX, brushY;
    for(int i = 0; i < bstips.getBlobsNumber(); i++){
      blobWidth = bstips.getBlobWidth(i);
      blobHeight = bstips.getBlobHeight(i);
      if(blobHeight > 20 || blobWidth > 20){
        brushX = bstips.getBoxCentX(i);
        brushY = bstips.getBoxCentY(i) - bstips.getBlobHeight(i)/2;
        pushMatrix();
        translate(20, 20);
        noFill();
        stroke(255);
        strokeWeight(2);
        ellipse(brushX, brushY, 20, 20);
        popMatrix();
      }
    }
    canvas.endDraw();
  }
  
  image(canvas, captureWidth+40, 20);    // Display the drawing.
}

void drawFingersTipsBoundingBox(){
   pushMatrix();
   translate(20, mainPartHeight+30);
   bstips.drawBox(color(255,0,0), 1);
   popMatrix();
}

void controlEvent(ControlEvent theControlEvent) {
  if(theControlEvent.isFrom("brushSizeRange")) {
    brushSize = int(theControlEvent.getController().getArrayValue(0));
    println("Brush set to: "+brushSize);
  }
}

public void reloadColor(int theValue) {
  brushColor = color(red(brushColor), green(brushColor), blue(brushColor), 255);
    colorDecay = 1.0;
    System.out.println("Renewed the color.");
}

public void clearImage(int theValue) {
     canvas.beginDraw();
    canvas.background(255);
    canvas.endDraw();
    mustSetBackground = true;
    System.out.println("Image cleared");
}

public void resetCamera(int theValue) {
    mustSetBackground = true;
    System.out.println("Camera re-set");
}
